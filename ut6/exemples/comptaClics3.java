// COMPTADOR DE CLICS 3: amb expressió LAMBDA
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


// En este cas, comptaClics3 és un JFrame i no necessita implementar ActionListener
public class comptaClics3 extends JFrame //implements ActionListener
{
	static int cont = 0;
	//private JLabel jl;	// es declara aquí per a poder utilitzar-la en tota la classe
	
	public comptaClics3()
	{
		JPanel jp = new JPanel(new GridLayout(0,1));
		// afegiré el jp al jf
		this.add(jp);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		// per a centrar el JFrame en pantalla
		this.setLocationRelativeTo(null);
		JLabel jl = new JLabel("Encara no has fet clic",SwingConstants.CENTER);
		JButton jb = new JButton("Fes me clic");
		// afegiré jl i jb al jp
		jp.add(jl);
		jp.add(jb);
		// dimensiona el JFrame en funció del seu contingut, SEMPRE DESPRÉS D'AFEGIR ELS COMPONENTS
		this.pack();
		this.setVisible(true);
		
		// interficieFuncional if = lambda;
		ActionListener al = e -> { cont++; jl.setText("Has fet " +  cont + " clics"); };
		
		jb.addActionListener(al);
	}
	
	public static void main(String args[])
	{	
		//JFrame jf = new JFrame("Comptador de clics");
		comptaClics2 cc2 = new comptaClics2();
	}
	
/*	public	void actionPerformed​(ActionEvent e)
	{
			// TODO write your own code here
			cont++;
			jl.setText("Has fet " +  cont + " clics");
	}	*/
}
