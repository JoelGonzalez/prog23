import javax.swing.*;

public class exJTextField
{
	public static void main(String args[]) throws InterruptedException
	{
			
		JFrame jf = new JFrame("Exemple de JTextField");
		jf.setVisible(true);
		jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		// per a centrar el JFrame en pantalla
		jf.setLocationRelativeTo(null);
		
		JTextField TextField = new JTextField("text inicial");
		// incloure el JTextField dintre del JFrame
		jf.getContentPane().add(TextField);
		jf.pack();
		// passats 10 segons (10000 ms) canvie el text
		Thread.sleep(10000);
		TextField.setText( "Un altre text diferent");
		//String content = textField.getText();
		
		
	}
}
