// Programa que accepta 2 números i, si la seva suma és inferior a 10, demana un tercer valor. A la fi, mostra la suma dels valors introduïts.

public class e6
{
    public static void main (String args[])
    {
        double x, y, suma;

        System.out.println("Introduce un número: ");
        x = Double.parseDouble(System.console().readLine());

        System.out.println("Introduce otro número: ");
        y = Double.parseDouble(System.console().readLine());
        suma = x+y;
        if ((suma) < 10)
        {
            double z;
            System.out.println("Introduce otro número: ");
            z = Double.parseDouble(System.console().readLine());
            suma = suma + z;    // suma += z
            //System.out.println("La suma de los números es: " + (x+y+z));
        }
        /*else
            System.out.println("La suma de los números es: " + (x+y));*/
        
        System.out.println("La suma és " + suma);
    }
}

// Hem afegit suma per a convertir l'alternativa doble en simple, però la solució amb la doble és perfecta també
