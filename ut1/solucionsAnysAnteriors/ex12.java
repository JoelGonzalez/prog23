
public class ex12 {
	public static void main (String[] args) {
		double n1;
		
		System.out.println("Introduzca un valor mayor que cero");
		n1=Double.parseDouble(System.console().readLine());
		while (n1<=0) {
			System.out.println("El valor "+n1+" introducido NO es correcto, por favor introduzca un valor mayor que cero");
			n1=Double.parseDouble(System.console().readLine());
		}
		System.out.println("El valor "+n1+" introducido es correcto");
	}
}
