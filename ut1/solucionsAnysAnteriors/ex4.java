/*4. Programa que demana un valor numèric i indica si aquest és positiu o negatiu.*/

public class ex4
{
    public static void main(String args[])
    {
        double num;
        System.out.println("Introdueix un numero. ");
        num = Double.parseDouble(System.console().readLine());

        if ( num > 0)
            System.out.println("El número és positiu");
        else
            if (num < 0)
                 System.out.println("El número és negatiu");
             else
                 System.out.println("El número és 0");            
    } 
}
