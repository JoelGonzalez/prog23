package trabajadores;

public class Empleado
{
	protected String nom;
	
	public Empleado(String n)
	{
		nom =n;
	}
	public void setNom(String n)
	{
		nom=n;
	}
	public String getNom()
	{
		return nom;
	}
	public String toString()
	{
		return "El nombre del empleado es "+nom;
	}
}
