package motors;

public class MotorElectrico extends Motor
{
	public String voltaje;
	public MotorElectrico()
	{
		super(3000);
		voltaje="220CA";
	}
	public MotorElectrico(double vel, String volt)
	{
		super(vel);
		voltaje=volt;
	}
	public MotorElectrico(MotorElectrico me)
	{
		super(me);
		voltaje=me.voltaje;
	}
	public void acelera()
	{
		velocidad = velocidad + 10;	// velocidad += 10;
		System.out.println("El motor ha acelerado");		
	}
	@Override
	public void gira()
	{
		System.out.println("El motor gira a " + velocidad + " rpm");
	}
}
