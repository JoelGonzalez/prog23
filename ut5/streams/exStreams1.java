// Exemple que compte positius des d'un array de doubles
import java.util.*;
import java.util.stream.*;

public class exStreams1
{
	public static void main(String[] args) {
		/*int nums[] = {7,2,-3,0,13,-8,9,10};

		// Generar un stream a partir d'un array
		long positius = Arrays.stream(nums).filter(n->n>=0).count();
		System.out.println("Han sigut " + positius + " positius.");*/
		
		long positius = Stream.of(7,2,-3,0,13,-8,9,10).filter( n -> n >= 0 ).count();
		System.out.println("Han sigut " + positius + " positius.");
		double mitjana = Stream.of(7,-2,3,-1,13,-8,9,-10).filter( n -> n >= 0 ).mapToInt(n->n).average().orElse(0);
		System.out.println("La mitjana aritmética dels positius és " + mitjana);
		
		
		// operació terminal forEach per a que mostre cada valor
		//Stream.of(7,2,-3,0,13,-8,9,10).filter( n -> n >= 0 ).forEach(n -> System.out.println(n));
		Stream.of(7,2,-3,0,13,-8,9,10).filter( n -> n >= 0 ).forEach( System.out::println);
	}
}
