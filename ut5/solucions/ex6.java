import java.io.*;
import java.util.*;

class compta
{
	public int contaParaules(File f) throws IOException
	{
		Scanner ent=new Scanner(f);
		int cont=0;
		
		while(ent.hasNext()==true)
		{
			ent.next(); //
			cont++;
		}
		return cont;
	}
}

public class ex6
{
	public static void main(String args[]) throws IOException
	{
		File f=new File(args[0]);
		
		compta c=new compta();
		System.out.println("El fitxer te "+ c.contaParaules(f) + " paraules.");
	}
}
	
