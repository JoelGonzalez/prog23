/* Exemple de TRANSACCIÓ (TOT  O RES)
 */
import java.sql.*;
public class ex7TransaccioEmpresa {
	
        public static void main(String[] args) {

        try ( Connection conn = DriverManager.getConnection("jdbc:mysql://172.17.0.3:3306/empresa","root","root");    	)
         {
		try (Statement stmt = conn.createStatement();)
		{
			// INICI DE LA TRANSACCIÓ
			conn.setAutoCommit(false);
			String sql = "update articulos set precio = precio -20 where id = 2";
			stmt.executeUpdate(sql);
			// Si l'id no existeix no llança l'excepció: anem a forçar que la llance
			sql = "SELECT COUNT(*) FROM articulos WHERE id = 6";
   
			    ResultSet rs = stmt.executeQuery(sql);
			    rs.next();
			    int cont = rs.getInt(1);
			    if (cont == 0) 
				throw new SQLException("La fila no existeix");
			// si cont és 0 no pot arribar aquí, aniria al catch corresponent de la línia 28
			sql = "update articulos set precio = precio + 20 where id = 6";
			stmt.executeUpdate(sql);
		}
		catch(SQLException se) {
			conn.rollback();	// desfà si fa falta el primer executeUpdate()
			conn.setAutoCommit(true);
			throw se;
        	}
        	// explícitament actualitze la base de dades amb commit()
		conn.commit();
		conn.setAutoCommit(true);
	}
	catch(Exception e)
	{
		System.out.println(e.getMessage());
	}
    }
}
