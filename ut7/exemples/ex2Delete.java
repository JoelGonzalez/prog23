/*
	 EJEMPLO DE DELETE
 */
import java.sql.*;

public class ex2Delete {
    
    public static void main(String[] args) {

        try ( Connection connexio = DriverManager.getConnection("jdbc:mysql://172.17.0.2:3306/musica","root","root");
        	Statement stmt = connexio.createStatement();)	
        {

            // Afegim dades a la taula vendedors
            String sql = "DELETE FROM musics where id = 5";
            stmt.executeUpdate(sql);
            
            System.out.println("Registro eliminado!");
            
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();

   	 }
   	}
    
}
