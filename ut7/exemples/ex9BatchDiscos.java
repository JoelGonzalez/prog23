/* Exemple de PROCESSAMENT PER LOTS (BATCH PROCESS)
 */
import java.sql.*;

public class ex9BatchDiscos {

        public static void main(String[] args) {
        try ( Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/musica","root","");
        	Statement stmt = conn.createStatement();) {
		// INICI DEl PROCESSAMENT BATCH
		conn.setAutoCommit(false);
		String sql = "insert into musics VALUES (4,'Duke Ellington','jazz')";
		stmt.addBatch(sql);
		sql = "insert into discos VALUES (6,'Take the A train',15,4)";
		stmt.addBatch(sql);
		stmt.executeBatch();
		//  commit()
		conn.commit();
		
        } catch(SQLException se) {
            //Errores de JDBC
            conn.rollback();
        }
        finally
        {
        	conn.setAutoCommit(true);
        }
    }
}
