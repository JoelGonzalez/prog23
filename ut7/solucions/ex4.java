/*4. Actualitzar el preu de 3 files incrementant-los un 5%, 6% i un 7%, respectivament.*/

import java.sql.*;

public class ex4{
    public static void main(String[] args){
        try(
            Connection cnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/empresa","root","");
            Statement stm = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stm.executeQuery("select * from articulos");
        ){
            //rs.beforeFirst();
            for(float i=5 ; i<8 ; i++){
                rs.next();
                rs.updateFloat("precio",(rs.getFloat(3)+(rs.getFloat(3)*(i/100))));
                rs.updateRow();
            }
        }catch(SQLException s){
            s.printStackTrace();
        }
    }
}
