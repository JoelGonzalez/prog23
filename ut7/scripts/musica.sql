-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Temps de generació: 18-05-2022 a les 00:10:36
-- Versió del servidor: 10.4.19-MariaDB
-- Versió de PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `musica`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `discos`
--

CREATE TABLE `discos` (
  `id` int(11) NOT NULL,
  `titol` varchar(50) NOT NULL,
  `music` int(11) NOT NULL,
  `preu` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Bolcament de dades per a la taula `discos`
--

INSERT INTO `discos` (`id`, `titol`, `music`, `preu`) VALUES
(1, 'Slow train coming', 1, 10),
(5, 'Street Legal', 1, 44),
(6, 'Take the A train', 15, 4);

-- --------------------------------------------------------

--
-- Estructura de la taula `musics`
--

CREATE TABLE `musics` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `genere` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Bolcament de dades per a la taula `musics`
--

INSERT INTO `musics` (`id`, `nom`, `genere`) VALUES
(1, 'Bob Dylan', 'pop-rock'),
(2, 'Pink Floyd', 'pop-rock'),
(4, 'Duke Ellington', 'jazz');

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `discos`
--
ALTER TABLE `discos`
  ADD PRIMARY KEY (`id`);

--
-- Índexs per a la taula `musics`
--
ALTER TABLE `musics`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
